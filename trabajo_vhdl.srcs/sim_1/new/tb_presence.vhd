-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 3.1.2022 00:23:17 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_PRESENCE is
end tb_PRESENCE;

architecture tb of tb_PRESENCE is

    component PRESENCE
        port (CLK      : in std_logic;
              STROBE   : in std_logic;
              SENSOR   : in std_logic;
              RST_STRB : out std_logic;
              P_OK     : out std_logic);
    end component;

    signal CLK      : std_logic;
    signal STROBE   : std_logic;
    signal SENSOR   : std_logic;
    signal RST_STRB : std_logic;
    signal P_OK     : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : PRESENCE
    port map (CLK      => CLK,
              STROBE   => STROBE,
              SENSOR   => SENSOR,
              RST_STRB => RST_STRB,
              P_OK     => P_OK);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        STROBE <= '0';
        SENSOR <= '0';
		wait for 11 ns;
        -- EDIT Add stimuli here
        SENSOR <= '1';
		wait for 11 ns;
        
        SENSOR <= '0';
		wait for 11 ns;
        
        STROBE <= '1';
		wait for 11 ns;
        
        SENSOR <= '1';
		wait for 11 ns;
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_PRESENCE of tb_PRESENCE is
    for tb
    end for;
end cfg_tb_PRESENCE;