-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 14.1.2022 22:07:10 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_fsm_cabin is
end tb_fsm_cabin;

architecture tb of tb_fsm_cabin is

    component fsm_cabin
        port (PEMER   : in std_logic;
              REARM   : in std_logic;
              CLK     : in std_logic;
              READY   : in std_logic;
              END_MOV : in std_logic;
              FIN     : out std_logic);
    end component;

    signal PEMER   : std_logic;
    signal REARM   : std_logic;
    signal CLK     : std_logic;
    signal READY   : std_logic;
    signal END_MOV : std_logic;
    signal FIN     : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : fsm_cabin
    port map (PEMER   => PEMER,
              REARM   => REARM,
              CLK     => CLK,
              READY   => READY,
              END_MOV => END_MOV,
              FIN     => FIN);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        PEMER <= '1';
        REARM <= '0';
        READY <= '0';
        END_MOV<='0';
		wait for 11 ns;
        -- EDIT Add stimuli here
        READY<='1';
        wait for 11 ns;
        READY <= '0';
        END_MOV<='1';
        wait for 11 ns;
        READY<='1';
        wait for 11 ns;
        PEMER<='0';
        wait for 11 ns;
        PEMER<='1';
        REARM<='1';
        wait for 11 ns;
        REARM<='0';
        READY<='1';
        wait for 11 ns;
        
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_fsm_cabin of tb_fsm_cabin is
    for tb
    end for;
end cfg_tb_fsm_cabin;