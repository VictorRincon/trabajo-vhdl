-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 2.1.2022 19:53:08 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_encoder is
end tb_encoder;

architecture tb of tb_encoder is

    component encoder
        port (code_i : in std_logic_vector (3 downto 0);
              code_o : out std_logic_vector (3 downto 0));
    end component;

    signal code_i : std_logic_vector (3 downto 0);
    signal code_o : std_logic_vector (3 downto 0);

begin

    dut : encoder
    port map (code_i => code_i,
              code_o => code_o);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        code_i <= (others => '0');
		wait for 11 ns;
        -- EDIT Add stimuli here
		code_i <= "0001";
		wait for 11 ns;
        code_i <= "0010";
		wait for 11 ns;
        code_i <= "0100";
		wait for 11 ns;
        code_i <= "1000";
		wait for 11 ns;
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_encoder of tb_encoder is
    for tb
    end for;
end cfg_tb_encoder;