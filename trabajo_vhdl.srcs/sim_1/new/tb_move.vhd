-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 2.1.2022 19:09:48 UTC

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tb_MOVE is
end tb_MOVE;

architecture tb of tb_MOVE is

    component MOVE
        port (CLK     : in std_logic;
              PEMER   : in std_logic;
              ENABLE  : in std_logic;
              READY   : in std_logic;
              CODE    : in std_logic_vector (3 downto 0);
              FLOOR_S : in std_logic_vector (3 downto 0);
              FIN     : out std_logic;
              MOTOR   : out std_logic_vector (1 downto 0));
    end component;

    signal CLK     : std_logic;
    signal PEMER   : std_logic;
    signal ENABLE  : std_logic;
    signal READY   : std_logic;
    signal CODE    : std_logic_vector (3 downto 0);
    signal FLOOR_S : std_logic_vector (3 downto 0);
    signal FIN     : std_logic;
    signal MOTOR   : std_logic_vector (1 downto 0);

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : MOVE
    port map (CLK     => CLK,
              PEMER   => PEMER,
              ENABLE  => ENABLE,
              READY   => READY,
              CODE    => CODE,
              FLOOR_S => FLOOR_S,
              FIN     => FIN,
              MOTOR   => MOTOR);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        PEMER <= '1';
        ENABLE <= '1';
        READY<='0';
        CODE <= (others => '1');
        FLOOR_S <= (others => '1');
		wait for 11 ns;
        -- EDIT Add stimuli here
        
        ENABLE <= '0';
        READY<='1';
        CODE <= "0100";
        FLOOR_S <= "0001";
        wait for 22 ns;
        
        FLOOR_S <= "0010";
        wait for 11 ns;
        
        FLOOR_S <= "0100";
        wait for 11 ns;
        
        CODE <= "0001";
        wait for 11 ns;
        
        FLOOR_S <= "0001";
        wait for 11 ns;
        
        CODE <= "0100";
        wait for 11 ns;
        
        PEMER<='0';
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_MOVE of tb_MOVE is
    for tb
    end for;
end cfg_tb_MOVE;
