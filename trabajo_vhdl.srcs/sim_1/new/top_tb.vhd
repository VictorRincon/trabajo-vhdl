-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 7.1.2022 11:11:14 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_top is
end tb_top;

architecture tb of tb_top is

    component top
        generic (
        	bits_floor: positive:=4;
      		CLKIN_FREQ : positive
   		 );
        port (CLK        : in std_logic;
              B_IN       : in std_logic_vector (3 downto 0);
              B_OUT      : in std_logic_vector (3 downto 0);
              PEMER      : in std_logic;
              REARM      : in std_logic;
              PRESENCE_S : in std_logic;
              DOOR_S     : in std_logic_vector (1 downto 0);
              FLOOR_S    : in std_logic_vector (3 downto 0);
              DOOR_A     : out std_logic_vector (1 downto 0);
              MOTOR      : out std_logic_vector (1 downto 0);
              segment    : out std_logic_vector (6 downto 0);
              UPDOWN     : out std_logic_vector (1 downto 0));
    end component;

    signal CLK        : std_logic;
    signal B_IN       : std_logic_vector (3 downto 0);
    signal B_OUT      : std_logic_vector (3 downto 0);
    signal PEMER      : std_logic;
    signal REARM      : std_logic;
    signal PRESENCE_S : std_logic;
    signal DOOR_S     : std_logic_vector (1 downto 0);
    signal FLOOR_S    : std_logic_vector (3 downto 0);
    signal DOOR_A     : std_logic_vector (1 downto 0);
    signal MOTOR      : std_logic_vector (1 downto 0);
    signal segment    : std_logic_vector (6 downto 0);
    signal UPDOWN     : std_logic_vector (1 downto 0);

    constant CLKIN_FREQ   : positive := 1_000;  -- Frequency for simulation
  	constant TbPeriod : time := 1 sec / CLKIN_FREQ;
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : top
    generic map (
      CLKIN_FREQ => CLKIN_FREQ
    )
    port map (CLK        => CLK,
              B_IN       => B_IN,
              B_OUT      => B_OUT,
              PEMER      => PEMER,
              REARM      => REARM,
              PRESENCE_S => PRESENCE_S,
              DOOR_S     => DOOR_S,
              FLOOR_S    => FLOOR_S,
              DOOR_A     => DOOR_A,
              MOTOR      => MOTOR,
              segment    => segment,
              UPDOWN     => UPDOWN);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        B_IN <= (others => '0');
        B_OUT <= (others => '0');
        PEMER <= '1';
        REARM <= '0';
        PRESENCE_S <= '0';
        DOOR_S <= (others => '0');
        FLOOR_S <= "0001";
        wait for 1.1 ms;

        -- EDIT Add stimuli here
        
        B_IN <= "1000";
        DOOR_S <="01" ;
        PRESENCE_S <= '1';
        wait for 1.1 ms;
        
        B_IN <= (others => '0');
        PRESENCE_S <= '0';
        wait for 6 ms;
        
        PRESENCE_S <= '1';
        wait for 1.1 ms;
        
        PRESENCE_S <= '0';
        DOOR_S <="01" ;
        wait for 7 ms;
        
        DOOR_S <="10" ;
        wait for 7 ms;
        
        FLOOR_S <= "1000";
        wait for 1.1 ms;
        
        DOOR_S <="01" ;
        wait for 7 ms;
        
        DOOR_S <="10" ;
        wait for 7 ms;
        
        B_IN <= "0001";
        wait for 1.1ms;
        
        B_IN <= (others => '0');
        wait for 6 ms;
        
        FLOOR_S <= "0001";
        wait for 1.1 ms;
        
        DOOR_S <="01" ;
        wait for 7 ms;
        
        DOOR_S <="10" ;
        wait for 7 ms;
        
        B_IN <= "0010";
        wait for 1.1ms;
        
        B_IN <= (others => '0');
        wait for 6 ms;
        
        B_IN <= "0001";
        wait for 1.1ms;
        
        B_IN <= (others => '0');
        wait for 6 ms;
        
        PEMER<='0';
        wait for 3 ms;
        
        PEMER<='1';
        REARM<='1';
        wait for 6 ms;
        
        DOOR_S <="01" ;
        B_IN <= "1000";
        wait for 6ms;
        
        DOOR_S <="10" ;
        B_IN <= (others => '0');
        wait for 6 ms;
        
        wait for 1000 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_top of tb_top is
    for tb
    end for;
end cfg_tb_top;