-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 14.1.2022 21:57:11 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_fsm_door is
end tb_fsm_door;

architecture tb of tb_fsm_door is

    component fsm_door
        port (PEMER    : in std_logic;
              CLK      : in std_logic;
              S_DOOR   : in std_logic_vector (1 downto 0);
              DOOR_A   : out std_logic_vector (1 downto 0);
              FIN      : in std_logic;
              PRESENCE : in std_logic;
              LISTO    : out std_logic);
    end component;

    signal PEMER    : std_logic;
    signal CLK      : std_logic;
    signal S_DOOR   : std_logic_vector (1 downto 0);
    signal DOOR_A   : std_logic_vector (1 downto 0);
    signal FIN      : std_logic;
    signal PRESENCE : std_logic;
    signal LISTO    : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : fsm_door
    port map (PEMER    => PEMER,
              CLK      => CLK,
              S_DOOR   => S_DOOR,
              DOOR_A   => DOOR_A,
              FIN      => FIN,
              PRESENCE => PRESENCE,
              LISTO    => LISTO);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        PEMER <= '1';
        S_DOOR <= (others => '0');
        FIN <= '0';
        PRESENCE <= '0';
		wait for 11 ns;
        -- EDIT Add stimuli here
        PRESENCE <= '1';
        S_DOOR <="01";
		wait for 11 ns;
        
        S_DOOR <="10";
		wait for 11 ns;
        
        FIN<='1';
		wait for 11 ns;
        
        S_DOOR <="01";
		wait for 11 ns;
        
        PRESENCE <= '1';
        S_DOOR <="01";
		wait for 11 ns;
        
        PEMER<='0';
		wait for 11 ns;
        
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_fsm_door of tb_fsm_door is
    for tb
    end for;
end cfg_tb_fsm_door;