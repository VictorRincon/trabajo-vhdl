-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;
entity fsm_door is
 port (
 PEMER : in std_logic;
 CLK : in std_logic;
 S_DOOR : in std_logic_vector(1 downto 0);
 DOOR_A : out std_logic_vector(1 downto 0);
 FIN : in std_logic;
 PRESENCE : IN STD_LOGIC;
 LISTO : out std_logic
 );
end fsm_door;

architecture behavioral of fsm_door is
 type STATES is (NOT_RDY, CLS, AUX, MOV, OPN);
 signal current_state: STATES := NOT_RDY;
 signal next_state: STATES;
 signal ANTICICLE: std_logic :='0';
begin
 state_register: process (PEMER, CLK)
 begin
 	if PEMER = '0' then
    	CURRENT_STATE <= NOT_RDY;
    elsif rising_edge(CLK) then
    	CURRENT_STATE <= NEXT_STATE;
    end if;
 end process;

 nextstate_decod: postponed process (PRESENCE,S_DOOR,FIN, current_state)
 begin
 	next_state <= current_state;
 	case current_state is
 	when NOT_RDY =>
 		if (PRESENCE='1' and S_DOOR="01") then
 			next_state <= CLS;
 		end if;
 	when CLS =>
 		if S_DOOR="10" then
 			next_state <= AUX;
 		end if;
 	when AUX =>
 		if FIN='0' then
 			next_state <= MOV;
 		end if;
 	when MOV =>
 		if FIN'EVENT AND FIN = '1' and ANTICICLE='1' then
 			next_state <= OPN;
 			ANTICICLE<= '0';
 		end if;
 		if ANTICICLE='0' then
        	ANTICICLE<= not ANTICICLE;
        end if;
	 when OPN =>
 		if S_DOOR="01" then
 			next_state <= NOT_RDY;
 		end if;
 	when others =>
 		next_state <= NOT_RDY;
 	end case;
 end process;
 output_decod: process (current_state)
 begin
 DOOR_A<= (OTHERS => '0');
 LISTO<='0';
 case current_state is
 when NOT_RDY =>
 DOOR_A<= (OTHERS => '0');
 LISTO<='0';
 when CLS =>
 DOOR_A(0) <= '1';
 DOOR_A(1) <= '0';
 when AUX =>
 DOOR_A<= (OTHERS => '0');
 LISTO <= '1';
 when MOV =>
 DOOR_A<= (OTHERS => '0');
 LISTO <= '1';
 when OPN =>
 DOOR_A(0) <= '0';
 DOOR_A(1) <= '1';
 LISTO<='0';
 when others =>
 DOOR_A<= (OTHERS => '0');
 LISTO<='0';
 end case;
 end process;
end behavioral;