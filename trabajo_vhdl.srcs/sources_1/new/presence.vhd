-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;

entity PRESENCE is
 port (
 CLK : in std_logic;
 STROBE: in std_logic; --strobe with 2 second period
 SENSOR: in std_logic; -- presence detector
 RST_STRB: out std_logic; -- reset the timer so it starts at cero (negative logic)
 P_OK: out std_logic --true when 2 seconds have elapsed without detecting presence
 );
end PRESENCE;

architecture behavioral of PRESENCE is
begin
process(CLK,SENSOR)
begin
	if SENSOR = '1' then
    	RST_STRB<='0';
        P_oK<='0';
    elsif rising_edge(CLK) then
        RST_STRB<='1';
        if STROBE='1' then
        	P_OK<='1';
        end if;
    end if;
end process;
end architecture;