
-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;
entity fsm_cabin is
 port (
 PEMER : in std_logic;
 REARM : in std_logic;
 CLK : in std_logic;
 READY : in std_logic;
 END_MOV: in std_logic;
 FIN: out std_logic
 );
end fsm_cabin;

architecture behavioral of fsm_cabin is
 type STATES is (REST, AUX, MOVE, EMER);
 signal current_state: STATES := REST;
 signal next_state: STATES;
 signal ANTICICLE: std_logic :='0';
 signal ANTICICLE2: std_logic :='0';
begin
 state_register: process (PEMER, CLK)
 begin
 	if PEMER = '0' then
    	CURRENT_STATE <= EMER;
    elsif rising_edge(CLK) then
    	CURRENT_STATE <= NEXT_STATE;
    end if;
 end process;
 nextstate_decod: postponed process (READY, END_MOV, current_state)
 begin
 	next_state <= current_state;
 	case current_state is
 	when REST =>
 		if READY = '1' then --and ANTICICLE2='1'
 			next_state <= AUX;
 		end if;
 		--if ANTICICLE2='0' then
        	--ANTICICLE2<=NOT ANTICICLE2;
        --end if;
 	when AUX =>
 		if END_MOV='0' then
 			next_state <= MOVE;
 		end if;
 	when MOVE =>
 		if END_MOV='1' and ANTICICLE='1' then
 			next_state <= REST;
            ANTICICLE<=NOT ANTICICLE;
 		end if;
        if ANTICICLE='0' then
        	ANTICICLE<=NOT ANTICICLE;
        end if;
	 when EMER =>
 		if PEMER='1' and REARM = '1' then
 			next_state <= REST;
 		end if;
 	when others =>
 		next_state <= REST;
 	end case;
 end process;
 output_decod: process (current_state)
 begin
 case current_state is
 when REST =>
 FIN <= '1';
 when AUX =>
 FIN <= '0';
 when MOVE =>
 FIN <= '0';
 when EMER =>
 FIN <= '1';
 when others =>
 FIN <= '1';
 end case;
 end process;
end behavioral;