library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_signed.all;

entity MOVE is
generic(BITS_FLOOR: POSITIVE:=4);
 port (
 CLK : in std_logic;
 PEMER: in std_logic;
 ENABLE : in std_logic; --negative logic. Movement allowed. Comes from fsm_cabin
 READY: in std_logic; --positive logic. Movement allowed. Comes from fsm_door
 CODE : in std_logic_vector(BITS_FLOOR-1 downto 0);  --FLOOR DESTINATION CODE
 FLOOR_S : in std_logic_vector(BITS_FLOOR-1 downto 0); --FLOOR SENSOR
 FIN: out std_logic;
 MOTOR: out std_logic_vector(1 downto 0) --MOTOR(0)==DOWN, MOTOR(1)==UP
 );
end MOVE;

architecture behavioral of MOVE is
begin
process(CLK,PEMER)
variable RESTA: integer range (-(2**BITS_FLOOR)) to 2**BITS_FLOOR := 0;
variable CODE_BUF: std_logic_vector(BITS_FLOOR-1 downto 0) :="1111";
variable FIN_A: std_logic:='1';
begin
	if CODE /= "1111" and FIN_A='1' then --when the code is not empty and
	                                     --movement has finished, refresh code
	   CODE_BUF:=CODE;
	end if;
	RESTA := conv_integer(CODE_BUF) - conv_integer(FLOOR_S);
	if PEMER ='0' then
    	FIN_A := '1';
    	CODE_BUF:="1111";
    	RESTA:=0;
        MOTOR<=(others=>'0');
    elsif CLK'event and CLK='1' then
    	--no change in movement should be made when the code is empty or the 
    	--cabin is between floors
    	if ENABLE ='0' and READY='1' and CODE_BUF/="1111" and FLOOR_S/="1111" then 
        	if RESTA > 0 then
            	MOTOR<="10";
                FIN_A:='0';
            elsif RESTA < 0 then
            	MOTOR<="01";
                FIN_A:='0';
            else 
            	MOTOR <= (others=>'0');
            	CODE_BUF:="1111";--when movement has finished, set the buffer to empty code
            	FIN_A:='1';
            end if;
        end if;
        FIN<=FIN_A;
    end if;
end process;
end architecture;