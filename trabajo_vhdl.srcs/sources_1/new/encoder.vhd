-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;

ENTITY encoder IS
	generic(BITS_FLOOR: POSITIVE:=4);
	PORT (
		code_i : IN std_logic_vector(BITS_FLOOR-1 DOWNTO 0);
		code_o : OUT std_logic_vector(BITS_FLOOR-1 DOWNTO 0)
	);
END ENTITY encoder;

ARCHITECTURE dataflow OF encoder IS
BEGIN
	WITH code_i SELECT
		code_o <= "0000" WHEN "0001",
			   "0001" WHEN "0010",
			   "0010" WHEN "0100",
			   "0011" WHEN "1000",
			   "1111" WHEN others;
END ARCHITECTURE dataflow;