----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31.12.2021 11:47:43
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity top is
generic(
    BITS_FLOOR: POSITIVE:=4;
    CLKIN_FREQ : positive := 100_000_000 -- Default frequency for synthesis;
);
port(
	-----IN-----
	CLK: in std_logic;
    B_IN: in std_logic_vector (BITS_FLOOR-1 downto 0);--buttons inside the cabin
    B_OUT: in std_logic_vector (BITS_FLOOR-1 downto 0);--buttons outside the cabin
    PEMER: in std_logic;--emergency button
    REARM: in std_logic;--rearm button
    PRESENCE_S: in std_logic;--presence detector
    DOOR_S: in std_logic_vector (1 downto 0);--cabin door sensors: DOOR_S(0)==START SWICH, DOOR_S(1)==END SWICH 													 --DOOR_S="01"=>COMPLETELY OPEN, DOOR_S="10"=>COMPLETELY CLOSED
    FLOOR_S: in std_logic_vector (BITS_FLOOR-1 downto 0);--floor sensors in the elevator shaft (one for each floor)
    
    -----OUT------
    DOOR_A: out std_logic_vector (1 downto 0);--cabin door actuators: DOOR_S(0)==RETRACT(OPEN), DOOR_S(1)==EXPAND(CLOSE)
    MOTOR: out std_logic_vector (1 downto 0);--motor actuators: MOTOR(0)==DOWN, MOTOR(1)==UP
    segment: out std_logic_vector (6 downto 0);--7 segment display 
    UPDOWN: out std_logic_vector (1 downto 0)--cabin going up/down indicator
);
end entity;

architecture structural of top is

component SYNCHRNZR is
port(
	CLK : in std_logic;
 	ASYNC_IN : in std_logic;
 	SYNC_OUT : out std_logic
 	);
end component;

component EDGEDTCTR is
port (
 	CLK : in std_logic;
 	SYNC_IN : in std_logic;
 	EDGE : out std_logic
 	);
end component;

component decoder IS
port(
	code : IN std_logic_vector(3 DOWNTO 0);
	led : OUT std_logic_vector(6 DOWNTO 0)
	);
end component;

component fsm_cabin is
port (
	PEMER : in std_logic;
 	REARM : in std_logic;
 	CLK : in std_logic;
 	READY : in std_logic;
 	END_MOV: in std_logic;
 	FIN: out std_logic
	);
end component;

component fsm_door is
 port (
 PEMER : in std_logic;
 CLK : in std_logic;
 S_DOOR : in std_logic_vector(1 downto 0);
 DOOR_A : out std_logic_vector(1 downto 0);
 FIN : in std_logic;
 PRESENCE : IN STD_LOGIC;
 LISTO : out std_logic
 );
end component fsm_door;

component TIMER is
  generic (
    MODULO  : positive
  );
  port ( 
    RST_N  : in   std_logic;
    CLK    : in   std_logic;
    STROBE : out  std_logic
  );
end component TIMER;

component MOVE is
 port (
 CLK : in std_logic;
 PEMER: in std_logic;
 ENABLE : in std_logic;
 READY: in std_logic;
 CODE : in std_logic_vector(BITS_FLOOR-1 downto 0);  --FLOOR DESTINATION CODE
 FLOOR_S : in std_logic_vector(BITS_FLOOR-1 downto 0); --FLOOR SENSOR
 FIN: out std_logic;
 MOTOR: out std_logic_vector(1 downto 0) --MOTOR(0)==DOWN, MOTOR(1)==UP
 );
end component MOVE;

component encoder IS
	PORT (
		code_i : IN std_logic_vector(BITS_FLOOR-1 DOWNTO 0);
		code_o : OUT std_logic_vector(BITS_FLOOR-1 DOWNTO 0)
	);
end component encoder;

component PRESENCE is
 port (
 CLK : in std_logic;
 STROBE: in std_logic; --strobe with 2 second period
 SENSOR: in std_logic; -- presence detector
 RST_STRB: out std_logic; -- reset the timer so it starts at cero
 P_OK: out std_logic --true when 2 seconds have elapsed without detecting presence
 );
end component PRESENCE;

component MUX is
PORT(
  INSIDE: IN STD_LOGIC_VECTOR (BITS_FLOOR-1 downto 0);
  OUTSIDE: IN STD_LOGIC_VECTOR (BITS_FLOOR-1 downto 0);
  CODE: OUT STD_LOGIC_VECTOR (BITS_FLOOR-1 downto 0)
);
end component;

--signals
type BUTTONS is array(1 downto 0) of std_logic_vector (BITS_FLOOR-1 DOWNTO 0);--includes both in and out buttons
signal ASYNC_IN: BUTTONS;
signal SYNC_IN: BUTTONS;
signal sync_out_door: std_logic_vector(1 downto 0);
signal EDGE_i: BUTTONS;
signal CODED: BUTTONS;
signal CODE_MUX: std_logic_vector(BITS_FLOOR-1 DOWNTO 0);
signal RST_STRB: std_logic;
signal strobe : std_logic;
signal PRSNC_OK: std_logic;
signal floor_n: std_logic_vector(BITS_FLOOR-1 DOWNTO 0);
signal door2cabin: std_logic;
signal move2cabin: std_logic;
signal cabin2door_move: std_logic;
SIGNAL AUX_UPDOWN: std_logic_vector(1 downto 0); 

begin
	ASYNC_IN<=(B_IN,B_OUT);
	--component instantiation (...)
    
    --synchronize and detect edges of all buttons
    in_out: for i in 0 to 1 generate
    	floor:for j in 0 to BITS_FLOOR-1 generate
          Sincronizador: SYNCHRNZR PORT MAP (
          CLK => clk,
          ASYNC_IN => ASYNC_IN(i)(j),
          SYNC_OUT => SYNC_IN(i)(j)
          );
          Detector_de_flanco: EDGEDTCTR PORT MAP (
          CLK => CLK,
          SYNC_IN => SYNC_IN(i)(j),
          EDGE => EDGE_i(i)(j)
          );
        end generate floor;
        encod: encoder port map(
        code_i => EDGE_i(i),
        code_o => CODED(i)
        );
    end generate in_out;
    
 Sincro_d_0: SYNCHRNZR PORT MAP (
      CLK => clk,
      ASYNC_IN => DOOR_S(0),
      SYNC_OUT => sync_out_door(0)
      );  
      
      Sincro_d_1: SYNCHRNZR PORT MAP (
      CLK => clk,
      ASYNC_IN => DOOR_S(1),
      SYNC_OUT => sync_out_door(1)
      );  
            
    mux_code: MUX port map(
      INSIDE=>CODED(0),
      OUTSIDE=>CODED(1),
      CODE=>CODE_MUX
    );
    
    timer1: TIMER
    generic map (
      MODULO  => CLKIN_FREQ *2
    )
    port map (
      RST_N => RST_STRB,
      CLK     => CLK,
      STROBE  => strobe
    );
    
    present: PRESENCE port map(
        CLK => CLK,
        STROBE => strobe,
        SENSOR => PRESENCE_S,
        RST_STRB => RST_STRB,
        P_OK => PRSNC_OK
    );
    
    encod_S: encoder port map(
        code_i => FLOOR_S,
        code_o => floor_n
    );
    
    decod: decoder port map(
        code => floor_n,
		led => segment
    );
    
    door: fsm_door port map(
        PEMER => PEMER,
        CLK => CLK,
        S_DOOR => sync_out_door,
        DOOR_A => DOOR_A,
        FIN => cabin2door_move,
        PRESENCE => PRSNC_OK, 
        LISTO => door2cabin
    );
    
    cabin: fsm_cabin port map(
        PEMER => PEMER,
        REARM => REARM,
        CLK => CLK,
        READY => door2cabin,
        END_MOV => move2cabin,
        FIN => cabin2door_move
    );
    
    movement: move port map(
        PEMER => PEMER,
        CLK => CLK,
        ENABLE => cabin2door_move,
        READY => door2cabin,
        CODE => CODE_MUX,
        FLOOR_S => floor_n,
        FIN => move2cabin,
        MOTOR => AUX_UPDOWN
    );
    
    MOTOR<=AUX_UPDOWN;
    UPDOWN<=AUX_UPDOWN;
end architecture;
